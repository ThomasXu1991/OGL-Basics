
#import <UIKit/UIKit.h>

#import "ObjCCrashcourseAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ObjCCrashcourseAppDelegate class]));
    }
}