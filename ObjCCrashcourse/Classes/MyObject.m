
#import "MyObject.h"

@implementation MyObject

- (id) init { 
    member1 = 0.1;
    member2 = 0.2;
    return self;
}

- (void) printString: (NSString *) string {
    NSLog(@"output: %@", string);
}

- (int) multiplyFirstValue: (int) value1
           withSecondValue: (int) value2
          andAddThirdValue: (int) value3 {
   
    return value1*value2+value3;
}

+ (int) multiply: (int) value1 with: (int) value2 {
    return value1*value2;
}

- (void)dealloc {
    NSLog(@"MyObject-instance will be deallocated.");
    [super dealloc];
}


@end
