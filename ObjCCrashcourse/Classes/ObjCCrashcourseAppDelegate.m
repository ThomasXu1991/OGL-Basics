
#import "ObjCCrashcourseAppDelegate.h"

@implementation ObjCCrashcourseAppDelegate

@synthesize window = _window;

// Note: You can ignore the Xcode console message "Application windows are expected to have a root view controller
// at the end of application launch". For simplicity most game projects in this book consist only of a single view.
// However, you can easily add a ViewController if you need it. See e.g. the GameKit or the GLKitBasics examples.

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];   
    [self.window makeKeyAndVisible];   

	MyObject *myObject = [[MyObject alloc] init];
    
    [myObject printString: @"Objective-C is cool!"];
    
    int result = [myObject multiplyFirstValue: 2
                              withSecondValue: 4
                             andAddThirdValue: 8];
    
    NSLog(@"result: %i", result);
    
    id myObject2Id = [MyObject alloc];
    MyObject *myObject2 = [myObject2Id init];
    [myObject2 printString: @"Alternative call."];
    
    int multiplyResult = [MyObject multiply: 16 with: 2];
    NSLog(@"multiplyResult : %i", multiplyResult);
    
    [myObject release];
    [myObject2 release];
    
    MyObject *test1 = [[MyObject alloc] init];
    MyObject *test2 = [[MyObject alloc] init];
    [test2 retain]; 
    NSLog(@"retainCount test1: %i", [test1 retainCount]);
    NSLog(@"retainCount test2: %i", [test2 retainCount]);
    [test1 release];
    [test2 release]; 
    NSLog(@"retainCount test2: %i", [test2 retainCount]);
    [test2 release]; 
	
    return YES;
}

- (void) applicationDidBecomeActive: (UIApplication *) application {}
- (void) applicationWillResignActive: (UIApplication *) application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {}

- (void)dealloc {
    [_window release];
    [super dealloc];
}

@end
