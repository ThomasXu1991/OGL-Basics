
@interface MyObject : NSObject {
    float member1;
    float member2;
}

- (id) init;

- (void) printString: (NSString *) string;

- (int) multiplyFirstValue: (int) value1
           withSecondValue: (int) value2
          andAddThirdValue: (int) value3; 

+ (int) multiply: (int) value1 with: (int) value2;

@end
