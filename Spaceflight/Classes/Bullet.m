
#import "Bullet.h"
#import "GameManager.h"

@implementation Bullet

/*
 Vertex x-Range from: -2.5 to: 2.5
 Vertex y-Range from: -2.5 to: 2.5
 Vertex z-Range from: -22.6 to: 27.4
 
 Suggested radius for bounding sphere: 10.8
 */ 

- (void) additionalSetup {
    type = BULLET;          
    radius = 10.8*2;
    
    GLfloat verticesObjData[ ] = { 
        0.0, -0.0, 27.4, -2.5, -2.5, -22.6, 2.5, -2.5, -22.6, 2.5, 2.5, -22.6, -2.5, 2.5, -22.6, 0.0, -0.0, -22.6
    };    
    
    GLfloat normalsObjData[ ] = {
        0.0, -0.1, 0.0, 0.0, -1.5, 0.1, 0.0, -1.5, 0.1, 1.5, 0.0, 0.1, 0.0, 1.5, 0.1, 0.0, 0.0, -1.0
    };
    
    GLushort facesObjData[ ] = { 
        0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 1, 1, 5, 2, 2, 5, 3, 3, 5, 4, 4, 5, 1
    };
    
    [self setVertices: verticesObjData
                 size: sizeof(verticesObjData) 
         setTexCoords: nil
                 size: 0
           setNormals: normalsObjData
                 size: sizeof(normalsObjData)
             setFaces: facesObjData
                 size: sizeof(facesObjData)];    
}

- (void) setColor {
    glColor4f(0, 0.7, 1, 1);
}

- (void) draw {   
    Player * p = [[GameManager getInstance] getPlayer];
    Vertex posPlayer = [p getPos]; 
    if (pos.z > posPlayer.z + [[GameManager getInstance] getzFar]) {
        active = false; 
    }  
    [super draw];
} 

@end