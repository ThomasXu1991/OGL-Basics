
#import "Sprite.h"

@interface Player : Sprite {
    CGPoint touchPoint;
    bool touchAction;
    float speedOffset;
    int w, h; 
    int hitCnt; 
    int timer;
}

- (void) setTouch: (CGPoint) p;
- (void) touchEnded;
- (void) hitCapsule;

@end
