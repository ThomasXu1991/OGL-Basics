
#import <UIKit/UIKit.h>
#import "Sprite.h"
#import "Tex.h"
#import "Asteroid.h"
#import "Obstacle.h"
#import "Capsule.h"
#import "Player.h"
#import "Bullet.h"
#import "Text.h"

extern float W; 
extern float H;

enum states {
    LOAD_GAME,
    PLAY_GAME,
    GAME_OVER
}; 

@interface GameManager : NSObject {
    int state; 	
    
    NSMutableArray *sprites; 
    NSMutableArray *newSprites; 
    NSMutableArray *destroyableSprites;   
    NSMutableDictionary* dictionary; 
     
    float zNear;
    float zFar; 
    float fieldOfViewAngle; 

    float xt;
    float yt;
    
    int timer;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;
- (id) createSprite: (int) type;

//Game Handler
- (Player *) getPlayer;
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) handleStates;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 
- (Vertex) getViewportOrigin;
- (void) checkSprite: (Sprite *) sprite;

//Helper Methods
- (void) setState: (int) stt;
- (int) getState;
- (float) getzFar;
- (void) manageSprites;
- (void) renderSprites;
- (int) getRndBetween: (int) bottom and: (int) top;
- (NSMutableDictionary*) getDictionary;
- (void) removeFromDictionary: (NSString*) name;

//OGL
- (void) setOGLProjection;
- (void) setLight;
- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag;
- (void) setCameraX: (GLfloat) camX 
            cameraY: (GLfloat) camY 
            cameraZ: (GLfloat) camZ
            lookAtX: (GLfloat) atX 
            lookAtY: (GLfloat) atY 
            lookAtZ: (GLfloat) atZ;
- (void) normalizeVector: (GLfloat [3]) v;
- (id) createOGLText: (NSString *) text 
             offsetX: (float) x
             offsetY: (float) y 
         selfDestroy: (bool) destroy;

@end
