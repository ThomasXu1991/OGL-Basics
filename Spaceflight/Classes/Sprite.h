
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>
#import "Tex.h"

enum types {    
    PLAYER,
    ASTEROID,
    OBSTACLE,
    CAPSULE,
    BULLET,
    TEXT
};

struct Vertex {
    float x, y, z;
};
typedef struct Vertex Vertex;

@interface Sprite : NSObject {    
    GLuint textureID;          
    Vertex pos;        
    Vertex speed;      
    Vertex rotation;    
    float angle;        
    float angleStep;    
    float radius;      
    int type;           
    bool active;        
    bool autoDestroy;  
    
    GLfloat *vertices;
    GLfloat *textureCoords;
    GLfloat *normals;
    GLushort *faces;
    
    int verticesLength;
    int facesLength;
}

- (void) additionalSetup;
- (int) getType;
- (Vertex) getPos;
- (float) getRadius;
- (void) setPos: (Vertex) newPos;
- (void) setSpeed: (Vertex) newSpeed;
- (void) setRotationStep: (float) newAngleStep 
              aroundAxis: (Vertex) newRotation;
- (bool) isActive;
- (void) draw;
- (void) move;
- (void) setColor;
- (void) renderSprite;
- (void) hit;
- (bool) checkColWithSprite: (Sprite *) sprite;
- (void) setVertices: (GLfloat *) verticesObjData
                size: (int) sizeVerts 
        setTexCoords: (GLfloat *) textureCoordsObjData
                size: (int) sizeTex
          setNormals: (GLfloat *) normalsObjData
                size: (int) sizeNorms
            setFaces: (GLushort *) facesObjData
                size: (int) sizeFaces;

@end
