
#import "Text.h"
#import "GameManager.h"

@implementation Text

- (void) additionalSetup {
    type = TEXT;    
}

- (void) setText: (NSString *) text 
         offsetX: (float) x
         offsetY: (float) y 
     selfDestroy: (bool) destroy {    
           
    Tex *tex = [[GameManager getInstance] getTex: text isImage: NO];
    textureID = [tex getTextureID];     
    
    w = [tex getWidth] / [tex getHeight]; 
    h = 1;

    GLfloat verticesObjData[ ] = {         
        0,    h, 0, 
        w,    h, 0, 
        0,    0, 0, 
        w,    0, 0  
    };    
    
    GLfloat textureCoordsObjData[ ] = { 
        1, 0, 0, 
        0, 0, 0, 
        1, 1, 0, 
        0, 1, 0  
    }; 
    
    GLfloat normalsObjData[ ] = {     
        0, 0, 1, 
        0, 0, 1,
        0, 0, 1,
        0, 0, 1
    };    
    
    [self setVertices: verticesObjData
                 size: sizeof(verticesObjData) 
         setTexCoords: textureCoordsObjData
                 size: sizeof(textureCoordsObjData)
           setNormals: normalsObjData
                 size: sizeof(normalsObjData)
             setFaces: nil
                 size: 0];   
    
    offsetX = x; 
    offsetY = y;
    selfDestroy = destroy;
}

- (void) move {           
    Vertex o = [[GameManager getInstance] getViewportOrigin];
    glTranslatef(o.x - w + offsetX, o.y - h + offsetY, o.z + 10);      
    
    if (selfDestroy) {
        timer++;
        if (timer > 30) {
            active = false;
        }    
    }
}

@end