
#import "Sprite.h"

@interface Text : Sprite {
    int timer;
    int w, h; 
    float offsetX; 
    float offsetY;
    bool selfDestroy; 
}

- (void) setText: (NSString *) text 
         offsetX: (float) x
         offsetY: (float) y
     selfDestroy: (bool) destroy;

@end
