
#import "Tex.h"

@interface ParallaxLayer : NSObject {    
    Tex *tex;
    
    int layerW;
    int layerH;
    
    float refX;
    float refY; 
    
    float oldPx;
    float oldPy;
}

- (id) initWithPic: (NSString *) picName;
/**
 *  <#Description#>
 *
 *  @param factor 确定平面相对玩家的运动速度，当fsctor
 *  @param pos    玩家的位置信息
 *  @param o      
 */
- (void) drawWithFactor: (float) factor 
             relativeTo: (CGPoint) pos
               atOrigin: (CGPoint) o;

@end