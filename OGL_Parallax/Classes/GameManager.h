
#import <UIKit/UIKit.h>
#import "Tex.h"
#import "ParallaxLayer.h"

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	
  
    NSMutableDictionary* dictionary; 
    
    Tex *playerTexture;
    int playerX;
    int playerY; 
    
    ParallaxLayer *back;
    ParallaxLayer *clouds;
    
    float xt;
    float yt;
    
    int timer;
    /**
     *  imgX,imgY用来记录点击的点，以供绘制图实用
     */
    float imgX;
    float imgY;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 
- (void) scrollWorld;
- (CGPoint) getViewportOrigin;

//Helper Methods
- (void) setState: (int) stt;
- (int) getRndBetween: (int) bottom and: (int) top;
- (NSMutableDictionary*) getDictionary;
- (void) removeFromDictionary: (NSString*) name;

//OGL
- (void) setOGLProjection;
- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2;
- (void) drawOGLRect: (CGRect) rect;
- (void) drawOGLImg: (NSString*) picName at: (CGPoint) p;
- (void) drawOGLString: (NSString*) text at: (CGPoint) p;
- (CGSize) getOGLImgDimension: (NSString*) picName;
- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag;

@end
