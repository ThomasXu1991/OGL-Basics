
#import <UIKit/UIKit.h>

extern int W; 
extern int H;

@interface MainView : UIView {  
}

- (void) rotateImage: (NSString*) picName 
                   x: (int) x 
                   y: (int) y 
               angle: (int) a;
- (int) getRndBetween: (int) bottom and: (int) top;
- (float) getRad: (float) grad;
- (float) getGrad: (float) rad;

@end