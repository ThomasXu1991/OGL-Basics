
#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;

- (BOOL) application: (UIApplication *) application didFinishLaunchingWithOptions: (NSDictionary *) launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor whiteColor];   
    GLKitViewController *vc = [[GLKitViewController alloc] init];
    self.window.rootViewController = vc;
    [vc release];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void) applicationWillResignActive: (UIApplication *) application {}
- (void) applicationDidEnterBackground:  (UIApplication *) application {}
- (void) applicationWillEnterForeground:(UIApplication *) application {}
- (void) applicationDidBecomeActive: (UIApplication *) application {}
- (void) applicationWillTerminate: (UIApplication *) application {}

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

@end
