
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface GLKitViewController : GLKViewController {
    EAGLContext *eaglContext;
}

- (void) setOGLProjection;
- (void) drawOGLTriangle;

@end
