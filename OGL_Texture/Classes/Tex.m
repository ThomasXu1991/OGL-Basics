
#import "Tex.h"

@implementation Tex

- (void) createTexFromImage: (NSString *) picName {     
    UIImage *pic = [UIImage imageNamed: picName];
	
	int scaleFactor = 1; 
	
	float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iOSVersion >= 4.0) {
        if (pic.scale >= 2) {
            scaleFactor = pic.scale;
        }
    } 
	
    if (pic) {
        //Texturabmessungen festlegen
        width = pic.size.width * scaleFactor;
        height = pic.size.height * scaleFactor;
        //判断宽高至少两个像素，不超过2048
        if ( (width & (width-1)) != 0 || (height & (height-1)) != 0
            || width > 2048 || height > 2048) {
            NSLog(@"ERROR: %@ width und/oder height ist keine 2er Potenz oder > 2048!", picName); 
        }  
        
        //Pixeldaten erzeugen
        GLubyte *pixelData = [self generatePixelDataFromImage: pic];    
    
        //Aus den Pixeldaten die Textur erzeugen und als ID speichern
        [self generateTexture: pixelData]; 
                
        //Cleanup                                                                                                                                                   
        int memory = width*height*4;
        NSLog(@"%@-Pic-Textur erzeugt, Size: %i KB, ID: %i", picName, memory/1024, textureID);
        free(pixelData);
						
		width /= scaleFactor;
        height /= scaleFactor;
    } else {
         NSLog(@"ERROR: %@ nicht gefunden, Textur nicht erzeugt.", picName);
    }
}

- (void) createTexFromString: (NSString *) text {
    //Texturabmessungen festlegen
    int len = [text length]*20;
    if (len < 64) width = 64;
    else if (len < 128) width = 128;
    else if (len < 256) width = 256;
    else width = 512; //max width text    
    height = 32;    
    
    //Pixeldaten erzeugen
    GLubyte *pixelData = [self generatePixelDataFromString: text];
        
    //Aus den Pixeldaten die Textur erzeugen und als ID speichern
    [self generateTexture: pixelData]; 
    
    //Cleanup
    int memory = width*height*4;
    NSLog(@"%@-Text-Textur erzeugt, Size: %i KB, ID: %i", text, memory/1024, textureID);
    free(pixelData);
}

- (GLubyte *) generatePixelDataFromImage: (UIImage *) pic {
    //calloc(<#size_t#>, <#size_t#>)获取内存
    GLubyte *pixelData = (GLubyte *) calloc( width*height*4, sizeof(GLubyte) );
    //获取颜色空间
    CGColorSpaceRef imageCS = CGImageGetColorSpace(pic.CGImage);
    //创建新的位图环境 pixelData为
    CGContextRef gc = CGBitmapContextCreate( pixelData,
                                             width, height, 8, width*4,                                                       
                                             imageCS, 
                                             kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(gc, CGRectMake(0, 0, width, height), pic.CGImage); //Render pic auf gc  
    CGContextRelease(gc);    
    return pixelData;    
}

- (GLubyte *) generatePixelDataFromString: (NSString *) text {   
    const char *cText = [text cStringUsingEncoding: NSASCIIStringEncoding];
    GLubyte *pixelData = (GLubyte *) calloc( width*height*4, sizeof(GLubyte) );
    CGColorSpaceRef rgbCS = CGColorSpaceCreateDeviceRGB();
    CGContextRef gc = CGBitmapContextCreate( pixelData, 
                                             width, height, 8, width*4,                                                       
                                             rgbCS, 
                                             kCGImageAlphaPremultipliedLast);        
    int size = 22; //Font-Groesse, kleiner als height
    CGContextSetRGBFillColor(gc, 0,1,0,1); //Schriftfarbe
    CGContextSelectFont(gc, "Verdana", size, kCGEncodingMacRoman);
    int ys = height-size; //swapped y-axis
    CGContextShowTextAtPoint(gc, 0, ys, cText, strlen(cText)); //Render text auf gc   
    CGColorSpaceRelease(rgbCS);
    CGContextRelease(gc);    
    return pixelData;
} 

- (void) generateTexture: (GLubyte *) pixelData {
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //创建纹
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData);
    
    //Textur-bezogene States global aktivieren
    glEnable(GL_BLEND);
    //    GL_ONE_MINUS_SRC_ALPHA混合函数，可实现与其他透明表面间的交叉混合操作
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    //    渲染过程中激活纹理坐标数组
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    //    GL_CLAMP_TO_EDGE纹理坐标映射至顶点上
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

- (void) drawAt: (CGPoint) p {
    //原始矩阵尺寸
    GLshort imageVertices[ ] = {
        0,      height, //links unten
        width,  height, //rechts unten
        0,      0,      //links oben
        width,  0       //rechts oben    
    };        
    
    GLshort textureCoords[ ] = {                         
        0, 1, //links unten
        1, 1, //rechts unten
        0, 0, //links oben
        1, 0  //rechts oben      
    };
    
	p.x = (int) p.x;
    p.y = (int) p.y;
	
    glEnable(GL_TEXTURE_2D); //alle Flaechen werden nun texturiert   
    
    glColor4f(1, 1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, textureID); 
    glVertexPointer(2, GL_SHORT, 0, imageVertices);	
    glTexCoordPointer(2, GL_SHORT, 0, textureCoords);    
    
    glPushMatrix();
    glTranslatef(p.x, p.y, 0);    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); 
    glPopMatrix();
    
    glDisable(GL_TEXTURE_2D);    
}

- (GLuint) getTextureID {
    return textureID;
}

- (int) getWidth {
    return width;
}

- (int) getHeight {
    return height;
}

- (void) dealloc {
    NSLog(@"Delete texture, ID: %i", textureID);
    glDeleteTextures(1, &textureID);
    [super dealloc];
}

@end
