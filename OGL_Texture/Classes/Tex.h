
#import <OpenGLES/ES1/gl.h>

@interface Tex : NSObject {
    GLuint textureID;
    int width;
    int height;
}

//Initialisierer
/**
 *  创建新纹理时，接受图像名
 *
 *  @param picName <#picName description#>
 */
- (void) createTexFromImage: (NSString *) picName;
- (void) createTexFromString: (NSString *) text;

//Texturerzeugung - Helper
- (GLubyte *) generatePixelDataFromImage: (UIImage *) pic;
- (GLubyte *) generatePixelDataFromString: (NSString *) text;
- (void) generateTexture: (GLubyte *) pixelData;

//Textur rendern
- (void) drawAt: (CGPoint) p;

//Getter
- (GLuint) getTextureID;
- (int) getWidth;
- (int) getHeight;

@end
