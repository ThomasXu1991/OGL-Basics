
#import "GameManager.h"

int W=320;
int H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
        NSLog(@"gameManager Singleton angelegt!");
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {        
    [self setOGLProjection];    
    state = LOAD_GAME;
}

- (void) loadGame { 
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) touchEnded {
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            break;           
        default: NSLog(@"ERROR: Unbekannter Spielzustand: %i", state);
            break;
    }    
}	 

- (void) playGame {
    timer++;
    /**
     1、绘制图片
     */
    Tex *tex = [[Tex alloc] init];
    [tex createTexFromImage:@"player.png"];
    static int yStep = 0;
    static int isUp = 1;
    if (((100 + yStep) < 0) ) {
        isUp = -1;
    }else if( ((100 + yStep) > H)){
        isUp = 1;
    }
    yStep -= 5*isUp;
    [tex drawAt:CGPointMake(100, 100 + yStep)];
    
    /**
     2、绘制Text
     */
    Tex *tex2 = [[Tex alloc] init];
    [tex2 createTexFromString:@"Hello!"];
    [tex2 drawAt:CGPointMake(100, 100)];
    
//    CGSize textureSize = [self getOGLImgDimension: @"player.png"];
//    int w = textureSize.width;
//    int h = textureSize.height;    
//    
//    static int yStep = 0;    
//    yStep -= 1; 
//    
//    for (int x = 0; x < W; x += w) {
//        for (int y = 0; y < H; y += h) {    
//            [self drawOGLImg: @"player.png" at: CGPointMake(x, y + yStep)]; 
//        }
//    }        
//    
//    [self drawOGLString: @"OpenGL ES rules!" at: CGPointMake(60, 100)];
}

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {
    //Set View
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();    
    glOrthof(0, W, H, 0, 0, 1); //2D-Perspektive
    
    //Origin = Oben links 
    //Positive x-Achse zeigt nach rechts
    //Positive y-Achse zeigt nach unten
    
    //Enable Modelview: Auf das Rendern von Vertex-Arrays umschalten
    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY); 
    glDisable(GL_DEPTH_TEST); //2D only
} 

- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2 {
    GLshort vertices[ ] = { 
        p1.x, p1.y,
        p2.x, p2.y        
    };      

    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    glDrawArrays(GL_LINES, 0, 2);       
}

- (void) drawOGLRect: (CGRect) rect {    
    GLshort vertices[ ] = {
        0,                  rect.size.height, //links unten
        rect.size.width,    rect.size.height, //rechts unten
        0,                  0, //links oben
        rect.size.width,    0  //rechts oben
    };
        
    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    
    glPushMatrix();      
    glTranslatef(rect.origin.x, rect.origin.y, 0);          
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); 
    glPopMatrix();      
}

- (void) drawOGLImg: (NSString*) picName at: (CGPoint) p {    
    Tex *tex = [self getTex: picName isImage: YES]; 
    if (tex) {
        [tex drawAt: p];
    }
}

- (CGSize) getOGLImgDimension: (NSString*) picName {    
    Tex *tex = [self getTex: picName isImage: YES]; 
    if (tex) {
        return CGSizeMake([tex getWidth], [tex getHeight]);
    }
    return CGSizeMake(0, 0); 
}

- (void) drawOGLString: (NSString*) text at: (CGPoint) p {    
    Tex *tex = [self getTex: text isImage: NO]; 
    if (tex) {
        [tex drawAt: p];
    }
}

- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag { 
	Tex *tex = [[self getDictionary] objectForKey: name];
	if (!tex) {
		tex = [[Tex alloc] init];
        if (imgFlag) {            
            [tex createTexFromImage: name];             
        } else {
            [tex createTexFromString: name];
        }    
        [[self getDictionary] setObject: tex forKey: name];
        NSLog(@"%@ als Tex im Dictionary abgelegt.", name);
        [tex release];         
	}          
	return tex;  
}

#pragma mark ============================= Helper Methods ===============================

- (void) setState: (int) stt {
    state = stt;
}

- (NSMutableDictionary *) getDictionary {
	if (!dictionary) { //Hashtable
		dictionary = [[NSMutableDictionary alloc] init]; 
		NSLog(@"Dictionary angelegt!");
	}
	return dictionary;
}

- (void) removeFromDictionary: (NSString*) name {
    [[self getDictionary] removeObjectForKey: name];
} 

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {        
    [dictionary release];
    [super dealloc];
}

@end
