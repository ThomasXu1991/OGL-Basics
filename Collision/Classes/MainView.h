
#import <UIKit/UIKit.h>
#import "Sprite.h"
#import "Zombie.h"

extern int W; 
extern int H;

@interface MainView : UIView {   
    NSMutableArray *sprites;
}

- (int) getRndBetween: (int) bottom and: (int) top;

@end