
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MainView.h"

@interface MyGameAppDelegate : NSObject <UIApplicationDelegate> {
    MainView *mainView;    
    id timer; //OS < 3.1 instanceOf NSTimer else instanceOf CADisplayLink   
}

@property (strong, nonatomic) UIWindow *window;

- (void) startGameLoop;
- (void) stopGameLoop;
- (void) loop;

@end

