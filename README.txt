
Thomas Lucka: iOS Game Development: Developing Games for iPad, iPhone, and iPod Touch (A K Peters / CRC)

Version: 2.0.2

The SRC-folder contains all Xcode projects covered in the book. More than 30 apps, which can be deployed straight to your iOS device. It is recommended to always use the latest Xcode and iOS version. Beta versions will not be explicitely supported. More details about installation of the examples can be found in chapter 2. 

Companion book site:
http://www.qioo.de/projects/book/iosgames/

===========================================================
Send feedback to: iphonebook@qioo.de 
===========================================================


{Last compiled with Xcode 4.6.3 on Mac OS 10.7.5, 20131008}

Note: Previous versions still work, but might need little adaption in the Xcode project settings in order to run on newer versions.


===========================================================

GERMAN / DEUTSCH:

Thomas Lucka: Spiele entwickeln fuer iPad, iPhone und iPod touch (Hanser Verlag)

Version: 2.0.2

Der SRC-Ordner enthaelt alle Beispielprogramme aus dem Buch. Mehr als 30 Apps, die direkt auf einem Mac oder einem iOS Device gestartet werden koennen. Beta-Versionen von Xcode oder dem iOS SDK werden nicht unterstuetzt. Weitere Hinweise zur Installation der Beispiele finden Sie im Abschnitt "Grundlagen", Kapitel 4.2 ff.  

Website zum Buch:
http://www.qioo.de/projekte/buch/iphonegames

===========================================================
Bei Fragen, Problemen, Anregungen: iphonegames_buch@qioo.de 
===========================================================



{Zuletzt kompiliert mit Xcode 4.6.3 unter Mac OS 10.7.5, 20131008}

Hinweis: Fruehere Versionen sind ebenfalls lauffaehig, muessen aber gegebenenfalls ueber die Projekteinstellungen minimal angepasst werden.
