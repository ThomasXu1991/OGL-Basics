
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Sprite.h"

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	    
    UIImage *background;
    
    NSMutableArray *sprites; 
    NSMutableArray *newSprites;
    NSMutableArray *destroyableSprites;    
    NSMutableDictionary* dictionary; 
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;
- (void) createSprite: (int) type 
                speed: (CGPoint) sxy 
                  pos: (CGPoint) pxy;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 
- (void) checkSprite: (Sprite *) sprite;

//Helper Methods
- (void) setState: (int) stt;
- (void) manageSprites;
- (void) renderSprites;
- (int) getRndBetween: (int) bottom and: (int) top;
- (void) drawString: (NSString *) str at: (CGPoint) p;
- (NSMutableDictionary*) getDictionary;
- (UIImage *) getPic: (NSString*) picName;
- (AVAudioPlayer *) getSound: (NSString *) soundName;
- (void) playSound: (NSString *) soundName;
- (void) loopSound: (NSString *) soundName;
- (void) stopSound: (NSString *) soundName;
- (void) saveObject: (id) object key: (NSString *) key;
- (id) readObjectforKey: (NSString *) key;

@end
