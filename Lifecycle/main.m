
#import <UIKit/UIKit.h>

#import "LifecycleAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LifecycleAppDelegate class]));
    }
}
