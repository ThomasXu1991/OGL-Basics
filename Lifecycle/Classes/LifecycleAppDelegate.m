
#import "LifecycleAppDelegate.h"

@implementation LifecycleAppDelegate

// Note: You can ignore the Xcode console message "Application windows are expected to have a root view controller
// at the end of application launch". For simplicity most game projects in this book consist only of a single view.
// However, you can easily add a ViewController if you need it. See e.g. the GameKit or the GLKitBasics examples.

- (BOOL)              application: (UIApplication *) application 
    didFinishLaunchingWithOptions: (NSDictionary *) launchOptions {    
    NSLog(@"application:didFinishLaunchingWithOptions: called.");
    [window makeKeyAndVisible];    
    return YES;
}

- (void) applicationDidFinishLaunching: (UIApplication *) application {
    NSLog(@"applicationDidFinishLaunching: called.");
}

- (void) applicationWillResignActive: (UIApplication *) application {
    NSLog(@"applicationWillResignActive: called.");
}

- (void) applicationDidBecomeActive: (UIApplication *) application {
    NSLog(@"applicationDidBecomeActive: called.");
}

- (void) applicationDidReceiveMemoryWarning: (UIApplication *) application {
    NSLog(@"applicationDidReceiveMemoryWarning: called.");
}      

- (void) applicationWillTerminate:(UIApplication *) application {
    NSLog(@"applicationWillTerminate: called.");
}

- (void) dealloc {
    [window release];
    [super dealloc];
}

@end
