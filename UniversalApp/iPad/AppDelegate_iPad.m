
#import "AppDelegate_iPad.h"

@implementation AppDelegate_iPad

// Note: In newer Xcode versions the xib-File will not be generated automatically if you choose
// the "empty application" as starting template; instead you can later add a new "window" incl. the xib.

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {        
    
    NSLog(@"App launched on iPad!");
    viewController = [[ViewController alloc] init];    
    //[window addSubview: [viewController view]];
    
    window.rootViewController = viewController;
    
    [window makeKeyAndVisible];
	 return YES;
}

- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}

@end
