
#import "ViewController.h"

@implementation ViewController

// Deprectated since iOS 6.0, but still needed for older versions
- (BOOL) shouldAutorotateToInterfaceOrientation: (UIInterfaceOrientation) interfaceOrientation {        
    
    if (interfaceOrientation == UIInterfaceOrientationPortrait) {
        NSLog(@"Orientation: UIInterfaceOrientationPortrait"); 
    }  
    if (interfaceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        NSLog(@"Orientation: UIDeviceOrientationPortraitUpsideDown");  
    }  
    if (interfaceOrientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"Orientation: UIDeviceOrientationLandscapeRight");  
    }  
    if (interfaceOrientation == UIDeviceOrientationLandscapeLeft) {
        NSLog(@"Orientation: UIDeviceOrientationLandscapeLeft");   
    }  
    
    [mainView setNeedsDisplay];
    
    return YES;   
}

/*
// Available since iOS 6.0
- (BOOL) shouldAutorotate {
    return YES;
}
*/

- (void) viewDidLoad {          
    UIDevice * device = [UIDevice currentDevice];
    NSLog(@"Model: %@", device.model); 
    NSLog(@"OS Version: %@", device.systemVersion);
    
    mainView = [[MainView alloc] initWithFrame: [UIScreen mainScreen].applicationFrame]; 
    mainView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view = mainView; 
    [super viewDidLoad];    
}

- (void) viewDidUnload {
    [super viewDidUnload];
}

- (void) dealloc {
    [mainView release];
    [super dealloc];
}

@end
