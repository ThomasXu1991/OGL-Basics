
#import "MainView.h"

@implementation MainView

- (void) drawRect: (CGRect) rect {    
    int w = rect.size.width;
    int h = rect.size.height;
    NSLog(@"MainView -> drawRect: %ix%i", w, h);         
    
    CGContextRef gc = UIGraphicsGetCurrentContext();    
    CGContextSetRGBFillColor(gc, 1, 1, 1, 1);
    NSString *str = @"Hello little bird!";
    UIFont *uif = [UIFont systemFontOfSize:40];
	 [str drawAtPoint:CGPointMake(10, 10) withFont: uif];        
}

@end
