
#import "MainView.h"

@implementation MainView

#pragma mark ============================= Init Methods ===============================

/**
 *  为了在当前视图基础上获得OGL视图，简单的重写LayerClass方法
 *
 *  @return <#return value description#>
 */
+ (Class) layerClass {
    return [CAEAGLLayer class];
}

- (void) setupOGL {   
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *) self.layer; 
    eaglLayer.opaque = YES;
    
    //初始化OGL图形环境
    eaglContext = [[EAGLContext alloc] initWithAPI: kEAGLRenderingAPIOpenGLES1];
    if (!eaglContext || ![EAGLContext setCurrentContext: eaglContext]) {
        [self release];
    } else {
        //初始化3个缓冲区
        //Renderbuffer
        glGenRenderbuffersOES(1, &renderbuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, renderbuffer);
        
        //Framebuffer 
        glGenFramebuffersOES(1, &framebuffer);
        glBindFramebufferOES(GL_FRAMEBUFFER_OES, framebuffer);        
        glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, renderbuffer);
        
        //Graphic context  
        [eaglContext renderbufferStorage: GL_RENDERBUFFER_OES fromDrawable: eaglLayer];          
        glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &viewportWidth);
        glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &viewportHeight);
        
        //Depthbuffer (3D only)
        glGenRenderbuffersOES(1, &depthbuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, depthbuffer);       
        glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, viewportWidth, viewportHeight);                
        glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthbuffer);        
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, renderbuffer); //rebind
                 
        if (!gameManager) {
            gameManager = [GameManager getInstance];
        } 
    }               
}

#pragma mark ============================= Draw Method ===============================

- (void) drawRect: (CGRect) rect {
    //确定视口画面
    glViewport(0, 0, viewportWidth, viewportHeight);
    //确定渲染视口的颜色
    glClearColor(0.5, 0.5, 0.5, 1.0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
    //进行下一步的渲染操作
    [gameManager drawStatesWithFrame: rect];     
    
    [eaglContext presentRenderbuffer: GL_RENDERBUFFER_OES];      
}

#pragma mark ============================= Input Handling ===============================

- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event {	
    CGPoint p = [[touches anyObject] locationInView: self];     
    [gameManager touchBegan: p];  
}

- (void) touchesMoved: (NSSet *) touches withEvent: (UIEvent *) event {   
    CGPoint p = [[touches anyObject] locationInView: self];        
    [gameManager touchMoved: p];
}

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event {
    [gameManager touchEnded];     
}

- (void) touchesCancelled: (NSSet *) touches withEvent: (UIEvent *) event {
    [gameManager touchEnded];
}

-(void) dealloc {
    [gameManager release];
    //清空创建的缓冲区中的内容
    if (eaglContext) {
        glDeleteRenderbuffersOES(1, &depthbuffer);
        glDeleteFramebuffersOES(1, &framebuffer);         
        glDeleteRenderbuffersOES(1, &renderbuffer);  
        [eaglContext release];
    }    
    [super dealloc];
}

@end
