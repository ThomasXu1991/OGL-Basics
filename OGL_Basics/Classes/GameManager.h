
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>//访问OpenGL函数的各个类中需倒入ES1/g1.h

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	
    int timer;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 

//Helper Methods
- (void) setState: (int) stt;
- (int) getRndBetween: (int) bottom and: (int) top;

//OGL
- (void) setOGLProjection;
- (void) drawOGLTriangle;
- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2;
- (void) drawOGLRect: (CGRect) rect;

@end
