
#import <UIKit/UIKit.h>
#import "GameManager.h"
/**
 *  相关头文件
 */
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <QuartzCore/QuartzCore.h>

@interface MainView : UIView {   
    GameManager *gameManager; 
    
    EAGLContext *eaglContext; //获得OpenGL图像环境的访问能力（渲染管线）
    //针对缓冲区的变量
    GLuint renderbuffer;
    GLuint framebuffer;
    GLuint depthbuffer;
    //针对屏幕尺寸的变量
    GLint viewportWidth;
    GLint viewportHeight;  
}

- (void) setupOGL;

@end