
#import "GameManager.h"

int W=320;
int H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
        NSLog(@"gameManager Singleton angelegt!");
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {        
    [self setOGLProjection];    
    state = LOAD_GAME;
}

- (void) loadGame { 
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) touchEnded {
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            break;           
        default: NSLog(@"ERROR: Unknown state: %i", state);
            break;
    }    
}	 

- (void) playGame {    
    timer++;                 
    
//    for (int i = 0; i < 10; i++) {
        int x = [self getRndBetween: 0 and: W];
        int y = [self getRndBetween: 0 and: H];
    
//    [self drawOGLRect: CGRectMake(56, 366, 10, 10)];
        [self drawOGLRect: CGRectMake(x, y, 10, 10)];
//    }
    
    [self drawOGLLineFrom: CGPointMake(0, H/2) to: CGPointMake(W, H/2)];
    
    [self drawOGLLineFrom: CGPointMake(W/2 - 10, 3) to: CGPointMake(-W/2 + 10, 3)];
    [self drawOGLLineFrom: CGPointMake(3, H/2 - 50) to: CGPointMake(3, -H/2 + 60)];
    
//    [self drawOGLTriangle]; //OpenGL ES - Hello World
}

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {
    //Set View
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    //2D透视
    //glOrthof (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar);
    //忽略zNear、zFar将其设为默认值0和1
    glOrthof(0, W, H, 0, 0, 1);

    //    激活矩阵算法，GL_MODELVIEW
    glMatrixMode(GL_MODELVIEW);
    //    OpenGL ES的状态
    glEnableClientState(GL_VERTEX_ARRAY);
    //    禁用深度缓存区，3D才需要
    glDisable(GL_DEPTH_TEST); //2D only
} 

- (void) drawOGLTriangle {    
    GLshort vertices[ ] = {
        5,   250,
        250, 250, 
        5,   5,
        255,   5,
        255, 250,
        10,   5,
    };
    
//    glColor4f(1, 1, 0, 1);
    
    glVertexPointer(2, GL_SHORT, 0, vertices);
    //顶点现实方式BeginMode
    /**
     *  <#Description#>
     *
     *  @param mode  <#mode description#>
     *  @param first 首个元素的索引，当渲染所有数据时课设置为0
     *  @param count 定点数量
     *
     glDrawArrays (GLenum mode, GLint first, GLsizei count)
     */
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2 {
    GLshort vertices[ ] = { 
        p1.x, p1.y,
        p2.x, p2.y        
    };      

    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 0, 0, 1);
    glDrawArrays(GL_LINES, 0, 2);
}

- (void) drawOGLRect: (CGRect) rect {    
    GLshort vertices[ ] = {
        0,                  rect.size.height,
        rect.size.width,    rect.size.height, 
        0,                  0, 
        rect.size.width,    0  
    };
        
    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    
    //glPushMatrix和glPopMatrix可操作栈中矩阵
    glPushMatrix();      
    glTranslatef(rect.origin.x, rect.origin.y, 0);
//    glRotatef(8.0, rect.origin.x, rect.origin.y, 0);
//    glScalef(rect.origin.x, rect.origin.y, 0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();      
}

#pragma mark ============================= Helper Methods ===============================

- (void) setState: (int) stt {
    state = stt;
}

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {        
    [super dealloc];
}

@end
