
#import <Foundation/Foundation.h>
#import <Box2D/Box2D.h>
#import "Tex.h"

@interface TileB2 : NSObject {
    b2Body *body;    
    Tex *tex;
    int x, y, w, h;
}

- (id) initWithPic: (NSString *) picName 
               pos: (CGPoint) pxy;
- (void) draw;
- (void) removeBody;

@end
