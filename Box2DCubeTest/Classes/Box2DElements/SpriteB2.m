
#import "SpriteB2.h"

@implementation SpriteB2

- (void) additionalSetup {
    type = CUBE;
    body = [[Box2dWrapper getInstance] createDynamicBody: CGRectMake(pos.x, pos.y, frameW, frameH)];  
}

- (void) renderSprite { 
    b2Vec2 position = body->GetPosition();
    pos.x = position.x * B2_RATIO - frameW/2.0f;
    pos.y = -position.y * B2_RATIO - frameH/2.0f;
    
    float32 angleRad = body->GetAngle();
    angle = -angleRad / (b2_pi/180.0);
    
    [super renderSprite];
    
    if (!active) 
    {
        [self removeBody];
    }
}

- (void) removeBody {
    [[Box2dWrapper getInstance] getWorld]->DestroyBody(body);
}

- (void) dealloc {
    [self removeBody];
    [super dealloc];
}

@end