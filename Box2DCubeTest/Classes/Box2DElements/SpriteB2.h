
#import "Sprite.h"
#import <Box2D/Box2D.h>
#import "GameManager.h"
#import "Box2dWrapper.h"

@interface SpriteB2 : Sprite  {
    b2Body *body;        
}

- (void) removeBody;

@end
