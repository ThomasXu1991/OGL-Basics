
#import "TileB2.h"
#import "GameManager.h"
#import "Box2dWrapper.h"

@implementation TileB2

- (id) initWithPic: (NSString *) picName 
               pos: (CGPoint) pxy {   
    self = [super init];    
   
    x = pxy.x;
    y = pxy.y;   

    tex = [[GameManager getInstance] getTex: picName isImage: YES];
    w = [tex getWidth];
    h = [tex getHeight];
    
    body = [[Box2dWrapper getInstance] createStaticBody: CGRectMake(x, y, w, h)];
    
    return self;
}

- (void) draw {        
    [tex drawAt: CGPointMake(x, y)];
}

- (void) removeBody {
    [[Box2dWrapper getInstance] getWorld]->DestroyBody(body);
}

- (void) dealloc {
    [self removeBody];
    [super dealloc];
}

@end
