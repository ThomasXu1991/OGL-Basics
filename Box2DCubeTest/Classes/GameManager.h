
#import <UIKit/UIKit.h>
#import "Sprite.h"
#import "Tex.h"

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    START_GAME,
    PLAY_GAME,
    GAME_OVER
}; 

@interface GameManager : NSObject {
    int state; 	
    
    NSMutableArray *sprites; 
    NSMutableArray *newSprites; 
    NSMutableArray *destroyableSprites;     
    NSMutableDictionary* dictionary; 
    NSMutableArray *tiles; 
    
    int timer;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) handleStates;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 

//Helper Methods
- (void) setState: (int) stt;
- (void) manageSprites;
- (void) renderSprites;
- (int) getRndBetween: (int) bottom and: (int) top;
- (NSMutableDictionary*) getDictionary;
- (void) removeFromDictionary: (NSString*) name;

//OGL
- (void) setOGLProjection;
- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2;
- (void) drawOGLRect: (CGRect) rect;
- (void) drawOGLImg: (NSString*) picName at: (CGPoint) p;
- (void) drawOGLString: (NSString*) text at: (CGPoint) p;
- (CGSize) getOGLImgDimension: (NSString*) picName;
- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag;

@end
