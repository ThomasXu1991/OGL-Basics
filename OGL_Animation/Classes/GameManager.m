
#import "GameManager.h"

int W=320;
int H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {    
    octoTexture = [self getTex: @"octo_8f.png" isImage: YES];
    
    [self setOGLProjection];
    
    state = LOAD_GAME;
}

- (void) loadGame { 
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
    //Modify by Thomas [手指位置赋值]－－－start
    imgY = p.y;
    imgX = p.x;
    //Modify by Thomas [手指位置赋值]－－－start
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) touchEnded {
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            //伴随手指移动的图
            [self playGameWith:CGPointMake(imgX - 32, imgY)];
            break;           
        default: NSLog(@"ERROR: Unknown state: %i", state);
            break;
    }    
}	 

/**
 *  在点绘制图
 *
 *  @param point
 */
- (void) playGameWith:(CGPoint)point{
    static int frameNr = 0;
    static int frameW = 64;
    static int angle = 0;
    
    //根据timer选择第几张图
    if (timer % 7 == 0) {
        frameNr ++;
        if (frameNr > 7) {
            frameNr = 0;
        }
    }
    
    [octoTexture drawFrame: frameNr
                frameWidth: frameW
                     angle: angle
                        at: point];
}

- (void) playGame {
    timer++;        
    
    static int frameNr = 0;
    static int frameW = 64;
    static int angle = 0;
    static int x = 0;
    static int y = 0;
    
    //Modify by Thomas [增加碰撞💥边角返回]－－－start
    static int xback = 1;
    static int yback = 1;
    if (x > W-frameW || x < 0) {
        xback = -xback;
    }
    if (y > H-frameW || y < 0) {
        yback = -yback;
    }
    //Modify by Thomas [增加碰撞💥边角返回]－－－end
    if (timer % 3 == 0) {
        frameNr ++;
        if (frameNr > 7) {
            frameNr = 0;
        }
    }
    //Modify by Thomas [增加碰撞💥边角返回]－－－start
//    angle++;
//    x++;
//    y++;
    angle++;
    x+=xback;
    y+=yback;
    //Modify by Thomas [增加碰撞💥边角返回]－－－start
    
    [octoTexture drawFrame: frameNr
                frameWidth: frameW
                     angle: angle
                        at: CGPointMake(x, y)];    
} 

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {
    //Set View
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();    
    glOrthof(0, W, H, 0, 0, 1); 
    
    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY); 
    glDisable(GL_DEPTH_TEST); //2D only
} 

- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2 {
    GLshort vertices[ ] = { 
        p1.x, p1.y,
        p2.x, p2.y        
    };      

    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    glDrawArrays(GL_LINES, 0, 2);       
}

- (void) drawOGLRect: (CGRect) rect {    
    GLshort vertices[ ] = {
        0,                  rect.size.height, 
        rect.size.width,    rect.size.height, 
        0,                  0, 
        rect.size.width,    0 
    };
        
    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    
    glPushMatrix();      
    glTranslatef(rect.origin.x, rect.origin.y, 0);          
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); 
    glPopMatrix();      
}

- (void) drawOGLImg: (NSString*) picName at: (CGPoint) p {    
    Tex *tex = [self getTex: picName isImage: YES]; 
    if (tex) {
        [tex drawAt: p];
    }
}

- (CGSize) getOGLImgDimension: (NSString*) picName {    
    Tex *tex = [self getTex: picName isImage: YES]; 
    if (tex) {
        return CGSizeMake([tex getWidth], [tex getHeight]);
    }
    return CGSizeMake(0, 0); 
}

- (void) drawOGLString: (NSString*) text at: (CGPoint) p {    
    Tex *tex = [self getTex: text isImage: NO]; 
    if (tex) {
        [tex drawAt: p];
    }
}

- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag { 
	Tex *tex = [[self getDictionary] objectForKey: name];
	if (!tex) {
		tex = [[Tex alloc] init];
        if (imgFlag) {            
            [tex createTexFromImage: name];             
        } else {
            [tex createTexFromString: name];
        }    
        [[self getDictionary] setObject: tex forKey: name];
        [tex release];         
	}          
	return tex;  
}

#pragma mark ============================= Helper Methods ===============================

- (void) setState: (int) stt {
    state = stt;
}

- (NSMutableDictionary *) getDictionary {
	if (!dictionary) { //Hashtable
		dictionary = [[NSMutableDictionary alloc] init]; 
	}
	return dictionary;
}

- (void) removeFromDictionary: (NSString*) name {
    [[self getDictionary] removeObjectForKey: name];
} 

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {        
    [dictionary release];
    [super dealloc];
}

@end
