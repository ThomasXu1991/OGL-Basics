
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	
    int timer;
    //X对应Y的数值
    float touchedY;
    
    //Add by Thomas DrawText －－－start
    int width;
    int height;
    GLuint textureID;
    //Add by Thomas DrawText －－－end
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 

//Helper Methods
- (void) setState: (int) stt;
- (int) getRndBetween: (int) bottom and: (int) top;

//OGL
- (void) setOGLProjection;
- (void) draw2DGraphPlotter;
- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2;
- (void) drawOGLRect: (CGRect) rect;

@end
