
#import <UIKit/UIKit.h>
#import "Sprite.h"

extern int W; 
extern int H;

@interface MainView : UIView {   
    Sprite *zombie;
    NSMutableArray *sprites;
}

- (int) getRndBetween: (int) bottom and: (int) top;

@end