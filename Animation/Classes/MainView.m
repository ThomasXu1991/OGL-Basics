
#import "MainView.h"

int W=320;
int H=480;

@implementation MainView

- (void) drawRect: (CGRect) rect {      
    W = rect.size.width; 
    H = rect.size.height;
    
    if (!zombie) {
        zombie = [[Sprite alloc] initWithPic: @"zombie_4f.png" 
                                    frameCnt: 4
                                   frameStep: 5
                                       speed: CGPointMake(0, -2)
                                         pos: CGPointMake(W/2, 400)];          
    }  
    
    [zombie draw]; 
    
    //Invasion
    if (!sprites) {
        sprites = [[NSMutableArray alloc] initWithCapacity: 20];
    
        for (int i=0; i<20; i++) {         
            int fs = [self getRndBetween: 1 and: 10];
            int sy = [self getRndBetween: -3 and: -1];
            int px = [self getRndBetween: 0 and: W];
            int py = [self getRndBetween: H and: H+100];
            Sprite *sprite = [[Sprite alloc] initWithPic: @"zombie_4f.png" 
                                                frameCnt: 4 
                                               frameStep: fs
                                                   speed: CGPointMake(0, sy)
                                                     pos: CGPointMake(px, py)];     
            [sprites addObject: sprite];
            [sprite release];           
        } 
    }    
    
    for (Sprite *sprite in sprites){           
        [sprite draw]; 
    }       
}

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
}  

-(void) dealloc {
    [zombie release];
    [sprites release];
    [super dealloc];
}

@end
