
#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>

extern NSString *highscoreListName;

@interface GCHelper : UIViewController <GKLeaderboardViewControllerDelegate>

+ (GCHelper *) getInstance;
- (void) showMessage: (NSString*) message;
- (void) login;
- (void) reportScore: (int) score;
- (void) showHighscores;

@end
