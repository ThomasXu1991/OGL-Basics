
#import "MainView.h"

@implementation MainView

int W = 320;
int H = 480;

static int score = 0;

- (void) drawRect: (CGRect) rect {                 
    score++;      
    
    CGContextRef gc = UIGraphicsGetCurrentContext();
	CGContextSetRGBFillColor(gc, 0, 0, 0, 1); 	
    UIFont *uif = [UIFont systemFontOfSize: 40];      
    
    NSString *scoreInfo = [NSString stringWithFormat: @"Score: %i", score];
    [scoreInfo drawAtPoint: CGPointMake(60, 200) withFont: uif]; 
        
	CGContextSetRGBFillColor(gc, 0, 0, 1, 1); 
    
    NSString *login = [NSString stringWithFormat: @"Login"];
    [login drawAtPoint: CGPointMake(0, 0) withFont: uif]; 
    
    NSString *submit = [NSString stringWithFormat: @"Submit"];
    [submit drawAtPoint: CGPointMake(0, H-50) withFont: uif]; 
    
    NSString *show = [NSString stringWithFormat: @"Show"];
    [show drawAtPoint: CGPointMake(W-100, H-50) withFont: uif]; 
}

- (void)touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event {	
	UITouch *touch = [touches anyObject]; 
	CGPoint p = [touch locationInView: self];
    
    if (p.x < 120 && p.y < 50) {   
        [[GCHelper getInstance] login];             
    }  
    else if (p.x < 120 && p.y > H-50) {   
        [[GCHelper getInstance] reportScore: score];         
    } 
    else if (p.x > W-100 && p.y > H-50) {   
        [[GCHelper getInstance] showHighscores];
    } 
}

@end
