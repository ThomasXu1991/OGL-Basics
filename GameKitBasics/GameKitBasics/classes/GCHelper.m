
#import "GCHelper.h"

/*
 Important Note:
 This example will only work if you set up your own project in iTunes Connect!
*/

//This name must match with the one from iTunes Connect.
NSString *highscoreListName = @"com.qioo.GameKitBasics.Highscores";

@implementation GCHelper

static GCHelper *gcHelper;

+ (GCHelper *) getInstance  {        
    if (gcHelper == nil) {
        gcHelper = [[GCHelper alloc] init];
    }
    return gcHelper;
}

- (id) initWithNibName: (NSString *) nibNameOrNil bundle: (NSBundle *) nibBundleOrNil { 
    gcHelper = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil];
    return gcHelper;
}

- (void) showMessage: (NSString*) message {
	UIAlertView* alert= [[[UIAlertView alloc] initWithTitle: @"GC Helper Message" 
                                                    message: message 
                                                   delegate: NULL 
                                          cancelButtonTitle: @"OK" 
                                          otherButtonTitles: NULL] autorelease];
	[alert show];	
}

- (void) login {
	NSString *iOSVersion = [[UIDevice currentDevice] systemVersion];
	BOOL isSupported = ([iOSVersion compare: @"4.2" options: NSNumericSearch] != NSOrderedAscending);
	
	if (NSClassFromString(@"GKLocalPlayer") && isSupported) {
        if([GKLocalPlayer localPlayer].authenticated == NO) {
            [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler: ^(NSError *error) {
                if (error != NULL) {
                    [self showMessage: @"Error: Login failed."];
                } else {
                    [self showMessage: @"Login successful."];
                }
            }];
        } else {
            [self showMessage: @"Already logged in."];
        }      
    } else {
        [self showMessage: @"Please update to a newer iOS version. Game Center Service is not supported."];
    }
}

- (void) reportScore: (int) score {	
    GKScore *scoreUploader = [[[GKScore alloc] initWithCategory: highscoreListName] autorelease];	
	scoreUploader.value = (int64_t) score;
	[scoreUploader reportScoreWithCompletionHandler: ^(NSError *error) {
        if (error != NULL) {
            [self showMessage: @"Error: Upload of score failed."];
        } else {
            [self showMessage: @"Highscore submitted."];
        }
    }];  
}

- (void) showHighscores {    
    //Show Highscore view
    GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
    if (leaderboardController != NULL) {	 
        leaderboardController.category = highscoreListName;
        leaderboardController.leaderboardDelegate = self;        
        [self presentViewController: leaderboardController animated: YES completion: nil];
    } 
    
    //Load Highscores data 
    GKLeaderboard* leaderBoard= [[[GKLeaderboard alloc] init] autorelease];
	leaderBoard.category= highscoreListName;	
	[leaderBoard loadScoresWithCompletionHandler:  ^(NSArray *scores, NSError *error) {
         if(error != nil)
         {
             [self showMessage:
              [NSString stringWithFormat: @"Error: %@", [error localizedDescription]]];
         } 
    }];  
}

- (void) leaderboardViewControllerDidFinish: (GKLeaderboardViewController *) viewController {
	//Close Highscore view
    [self dismissModalViewControllerAnimated: YES];
	[viewController release];
}

@end
