
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GCHelper.h"
#import "MainView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    GCHelper *gcHelper;
    MainView *mainView;    
    id timer; //OS < 3.1 instanceOf NSTimer else instanceOf CADisplayLink  
}

@property (strong, nonatomic) UIWindow *window;

- (void) startGameLoop;
- (void) stopGameLoop;
- (void) loop;

@end
