
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>

extern float W; 
extern float H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	
 
    float zNear; 
    float zFar; 
    float fieldOfViewAngle; 
    
    int timer;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 

//Helper Methods
- (int) getRndBetween: (int) bottom and: (int) top;

//OGL
- (void) setOGLProjection;
- (void) drawLine;
- (void) drawTriangle;
- (void) drawRectangle;

@end
