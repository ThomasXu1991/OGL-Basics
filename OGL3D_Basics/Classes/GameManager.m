
#import "GameManager.h"

float W=320;
float H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {       
    [self setOGLProjection];    
    state = LOAD_GAME;
}

- (void) loadGame {         
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) touchEnded {
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];            
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            break;           
        default: NSLog(@"ERROR: Unknown state: %i", state);
            break;
    }    
}	 

- (void) playGame {
    timer++;     

    [self drawLine];
    [self drawTriangle];
    [self drawRectangle];
} 

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {    
    glLoadIdentity();
    
    //Set View
    glMatrixMode(GL_PROJECTION);     
    zNear = 0.1; 
    zFar = 2500;    
    fieldOfViewAngle = 45;
    float top = zNear * tan(M_PI * fieldOfViewAngle / W);
    float bottom = -top;
    float left = bottom * W / H; 
    float right = top * W / H;
    //glFrustumf函数，坐标系
    glFrustumf(left, right, bottom, top, zNear, zFar); 

    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY);    
    glEnable(GL_DEPTH_TEST); 
    glDepthFunc(GL_LESS);
} 

- (void) drawLine {     
    GLbyte vertices[ ] = { 
        0, 0, 
        1, 0      
    };     
    glPushMatrix();
    glColor4f(1, 1, 0, 1); 
    glVertexPointer(2, GL_BYTE, 0, vertices); 
    glTranslatef(0, 0, -zNear-0.0001);
    glDrawArrays(GL_LINES, 0, 2); 
    glPopMatrix();      
}

- (void) drawTriangle {
    GLbyte vertices[ ] = {
        -1,  1,  0, 
         1, -1,  0, 
         1,  1,  0  
    };          
    glPushMatrix(); 
    glColor4f(0, 1, 0, 1); 
    glVertexPointer(3, GL_BYTE, 0, vertices); 
    glTranslatef(0, 0, -6);   
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3); 
    glPopMatrix();
}

- (void) drawRectangle {      
    GLbyte vertices[ ] = {
        -1,  1,  0, 
         1,  1,  0, 
        -1, -1,  0, 
         1, -1,  0  
    };  
    glPushMatrix(); 
    glColor4f(0, 0, 1, 1); 
    glVertexPointer(3, GL_BYTE, 0, vertices); 
    glTranslatef(0, 0, -6);     
    static int angle = 0; angle += 2;
    glRotatef(angle, 1, 0, 0); 
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
}

#pragma mark ============================= Helper Methods ===============================

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {        
    [super dealloc];
}

@end
