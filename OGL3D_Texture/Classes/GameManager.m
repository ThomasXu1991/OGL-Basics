
#import "GameManager.h"

float W=320;
float H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {       
    [self setOGLProjection];
    state = LOAD_GAME;
}

- (void) loadGame {        
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) touchEnded {
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];            
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            break;           
        default: NSLog(@"ERROR: Unknown state: %i", state);
            break;
    }    
}	 

- (void) playGame {
    timer++;     

    [self drawTexture];
} 

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {    
    glLoadIdentity();
    
    //Set View
    glMatrixMode(GL_PROJECTION);     
    zNear = 0.1; 
    zFar = 2500;    
    fieldOfViewAngle = 45;
    float top = zNear * tan(M_PI * fieldOfViewAngle / W);
    float bottom = -top;
    float left = bottom * W / H; 
    float right = top * W / H;    
    glFrustumf(left, right, bottom, top, zNear, zFar); 

    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY);    
    glEnable(GL_DEPTH_TEST); 
    glDepthFunc(GL_LESS);
} 

- (void) drawTexture {        
    Tex *tex = [[GameManager getInstance] getTex: @"fighter.png" isImage: YES];
    GLuint textureID = [tex getTextureID];
    
    int w = [tex getWidth] / [tex getHeight];
    int h = 1;
    
    GLfloat vertices[ ] = {    
        -w,  h,  0, 
         w,  h,  0, 
        -w, -h,  0, 
         w, -h,  0 
    };         
    
    GLfloat textureCoords[ ] = {                         
        1, 0, 
        0, 0, 
        1, 1, 
        0, 1      
    };   
     
    int verticesLength = sizeof(vertices) / sizeof(GLfloat);
      
    glEnable(GL_TEXTURE_2D); 
    glVertexPointer(3, GL_FLOAT, 0, vertices);
    glBindTexture(GL_TEXTURE_2D, textureID);     
    glTexCoordPointer(2, GL_FLOAT, 0, textureCoords);      
    
    glColor4f(1, 1, 1, 1); 
    
    glPushMatrix();            
    
    static float y = -8; 
    y += 0.2; 
    if (y > 8) y = -8;
    glTranslatef(0, y, -16);
    
    static int angle = 0; 
    angle += 10;    
    glRotatef(angle, 0, 1, 0); 
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, verticesLength/3);     
    glPopMatrix();
    
    //Cleanup
    glDisable(GL_TEXTURE_2D);                 
}
 
- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag { 
	Tex *tex = [[self getDictionary] objectForKey: name];
	if (!tex) {
		tex = [[Tex alloc] init];
        if (imgFlag) {            
            [tex createTexFromImage: name];             
        } else {
            [tex createTexFromString: name];
        }    
        [[self getDictionary] setObject: tex forKey: name];
        [tex release];         
	}          
	return tex;  
}

#pragma mark ============================= Helper Methods ===============================

- (NSMutableDictionary *) getDictionary {
	if (!dictionary) { 
		dictionary = [[NSMutableDictionary alloc] init]; 
	}
	return dictionary;
}

- (void) removeFromDictionary: (NSString*) name {
    [[self getDictionary] removeObjectForKey: name];
} 

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {   
    [dictionary release];
    [super dealloc];
}

@end
