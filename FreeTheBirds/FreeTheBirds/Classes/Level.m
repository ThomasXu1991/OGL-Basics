
#import "Level.h"
#import "GameManager.h"

@implementation Level

- (void) setLevel {       
    int x = 50;    
    
    [[GameManager getInstance] addB2Sprite: BLUEBIRD at: CGPointMake(24, H-32)];
    [[GameManager getInstance] addB2Sprite: BLUEBIRD at: CGPointMake(70, H-32)];
    [[GameManager getInstance] addB2Sprite: BLUEBIRD at: CGPointMake(118, H-32)];

    [[GameManager getInstance] addB2Sprite: YELLOWBIRD at: CGPointMake(323-x, H-32*2-16)];
    [[GameManager getInstance] addB2Sprite: YELLOWBIRD at: CGPointMake(386-x, H-32*3)];
    [[GameManager getInstance] addB2Sprite: YELLOWBIRD at: CGPointMake(356-x, H-32*5)];
    
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(261-x, H-32)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(435-x, H-32)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(286-x, H-32*3-16)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(286-x, H-32*2-16)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(421-x, H-32*2-16)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(417-x, H-32*3-16)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(351-x, H-32*7)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(326-x, H-32)];
    [[GameManager getInstance] addB2Sprite: CUBE32x32 at: CGPointMake(388-x, H-32)];
        
    [[GameManager getInstance] addB2Sprite: CUBE64x16 at: CGPointMake(269-x, H-32-16)];
    [[GameManager getInstance] addB2Sprite: CUBE64x16 at: CGPointMake(335-x, H-32-16)];
    [[GameManager getInstance] addB2Sprite: CUBE64x16 at: CGPointMake(399-x, H-32-16)];
    [[GameManager getInstance] addB2Sprite: CUBE64x16 at: CGPointMake(304-x, H-32*4)];
    [[GameManager getInstance] addB2Sprite: CUBE64x16 at: CGPointMake(370-x, H-32*4)];
    [[GameManager getInstance] addB2Sprite: CUBE64x16 at: CGPointMake(332-x, H-32*6)];
         
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(360-x, H-16*4)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(360-x, H-16*5)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(360-x, H-16*6)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(360-x, H-16*7)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(392-x, H-16*4)];    
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(325-x, H-16*9)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(325-x, H-16*10)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(325-x, H-16*11)];    
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(389-x, H-16*9)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(389-x, H-16*10)];
    [[GameManager getInstance] addB2Sprite: CUBE16x16 at: CGPointMake(389-x, H-16*11)];     
}

@end
