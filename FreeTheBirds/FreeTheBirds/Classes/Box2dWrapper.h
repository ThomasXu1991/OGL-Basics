
#import <Foundation/Foundation.h>
#import <Box2D/Box2D.h>
#import "SpriteB2.h"

extern int32 B2_VELOCITY_ITERATIONS;
extern int32 B2_POSITION_ITERATIONS;
extern float32 B2_TIMESTEP;
extern int32 B2_RATIO;

@interface Box2dWrapper : NSObject {    
    b2World* world;    
}

+ (Box2dWrapper *) getInstance;
- (void) setup;
- (b2World* ) getWorld;
- (void) update;
- (b2Body *) createStaticBody: (CGRect) rect;
- (b2Body *) createDynamicBody: (CGRect) rect;

@end
