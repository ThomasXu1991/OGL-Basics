
#import "BlueBird.h"

@implementation BlueBird

- (void) additionalSetup {
    type = BLUEBIRD;
    [super additionalSetup];   
}

- (void) touchBegan: (CGPoint) point {     
    if ([self checkColWithPoint: point] && !used) {
        touchAction = true;    
    }
}

- (void) touchEnded: (CGPoint) point {
    if (touchAction) { 
        touchAction = false; 
        used = true;
        frameStep = 4; 

        float xf = (point.x - pos.x - frameW/2.0f) * 50; 
        float yf = (point.y - pos.y - frameH/2.0f) * -1 * 50; 
        
        body->ApplyForceToCenter(b2Vec2(xf / B2_RATIO, yf / B2_RATIO));
    }
}

@end
