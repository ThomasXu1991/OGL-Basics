
#import "YellowBird.h"

@implementation YellowBird

- (void) additionalSetup {
    type = YELLOWBIRD;
    [super additionalSetup];  
}

- (void) twinkle {
    frameStep = 4;
}

@end
