
#import "SpriteB2.h"

@interface BlueBird : SpriteB2 {
    bool touchAction;
    bool used;
}

- (void) touchBegan: (CGPoint) touchPoint;
- (void) touchEnded: (CGPoint) touchPoint;

@end
