
#import <UIKit/UIKit.h>
#import "GameManager.h"
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <QuartzCore/QuartzCore.h>

@interface MainView : UIView {   
    GameManager *gameManager; 
    
    EAGLContext *eaglContext; 
    GLuint renderbuffer; 
    GLuint framebuffer;
    GLuint depthbuffer; 
    GLint viewportWidth;
    GLint viewportHeight;  
}

- (void) setupOGL;

@end