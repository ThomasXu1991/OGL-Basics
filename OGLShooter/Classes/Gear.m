
#import "Gear.h"

@implementation Gear

- (void) draw {   
    if (active) {
        pos.x+=speed.x;
        pos.y+=speed.y;        
        [self drawFrame];
        if (cnt > 3) {
            active = false;
        }
    }    
}  

@end
