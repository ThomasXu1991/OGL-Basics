
#import "Animation.h"
#import "GameManager.h"
#import "Tex.h"

@implementation Animation

- (void) drawFrame {
    frameNr = [self updateFrame];
    if (cycleCnt == 1) {
        active = false;
    }  
    if (active) {        
        [self renderSprite];  	
    }    
}

+ (CGPoint) getOriginBasedOnCenterOf: (CGRect) rectMaster 
                              andPic: (NSString *) picName 
                        withFrameCnt: (int) fcnt {    
    Tex *slave = [[GameManager getInstance] getTex: picName isImage: YES];
    
    int xmm = rectMaster.origin.x + rectMaster.size.width/2;
    int ymm = rectMaster.origin.y + rectMaster.size.height/2;

    int xs = xmm-[slave getWidth]/2/fcnt;
    int ys = ymm-[slave getHeight]/2;

    return CGPointMake(xs, ys);
}

@end
