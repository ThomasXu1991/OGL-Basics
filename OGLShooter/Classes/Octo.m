
#import "Octo.h"
#import "GameManager.h"

@implementation Octo

- (void) additionalSetup {
    autoDestroy = true;
}

- (void) hit {
    active = false;
    [[GameManager getInstance] createExplosionFor: self];
}

@end