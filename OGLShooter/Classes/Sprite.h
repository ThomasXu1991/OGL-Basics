
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>
#import "Tex.h"

enum types {    
    PLAYER,    
    BULLET,
    GEAR,
    OCTO,
    MINE,
    FIGHTER,
    ANIMATION
};

@interface Sprite : NSObject {    
    Tex *tex;           
    CGPoint speed;      
    CGPoint pos;        
    int cnt;              
    int frameNr;       
    int frameCnt;            
    int frameStep;       
    int frameW;        
    int frameH;         
    int angle;          
    int type;           
    int tolBB;           
    int cycleCnt;      
    bool forceIdleness; 
    bool active;        
    bool autoDestroy;   
}

-(id) initWithPic: (NSString *) name 
         frameCnt: (int) fcnt 
        frameStep: (int) fstp
            speed: (CGPoint) sxy 
              pos: (CGPoint) pxy;
- (void) additionalSetup;
- (void) draw;
- (void) drawFrame;
- (void) renderSprite;
- (int) updateFrame;
- (CGRect) getRect;
- (bool) checkColWithPoint: (CGPoint) p;
- (bool) checkColWithRect: (CGRect) rect;
- (bool) checkColWithSprite: (Sprite *) sprite;
- (void) hit;
- (float) getRad: (float) grad;
- (void) setType: (int) spriteType;
- (int) getType;
- (void) setSpeed: (CGPoint) sxy;
- (CGPoint) getSpeed;
- (CGPoint) getPos;
- (bool) isActive;

@end
