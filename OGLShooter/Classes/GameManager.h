
#import <UIKit/UIKit.h>
#import "Sprite.h"
#import "Tex.h"
#import "Player.h"
#import "Octo.h"
#import "Bullet.h"
#import "Gear.h"
#import "Mine.h"
#import "Fighter.h"
#import "Animation.h"
#import "ParallaxLayer.h"

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    START_GAME,
    PLAY_GAME,
    GAME_OVER
}; 

@interface GameManager : NSObject {
    int state; 	
    Player *player;
    
    NSMutableArray *sprites; 
    NSMutableArray *newSprites; 
    NSMutableArray *destroyableSprites;      
    NSMutableDictionary* dictionary; 
    
    ParallaxLayer *back;
    ParallaxLayer *clouds;
    
    float xt;
    float yt;
    int timer;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;
- (id) createSprite: (int) type 
              speed: (CGPoint) sxy 
                pos: (CGPoint) pxy;
- (void) createExplosionFor: (Sprite *) sprite;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) handleStates;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 
- (void) scrollWorld;
- (CGPoint) getViewportOrigin;
- (void) generateNewEnemies;
- (void) generateEnemy: (int) type 
                speedX: (int) sx 
                speedY: (int) sy; 
- (CGPoint) getRndStartPos;
- (void) checkSprite: (Sprite *) sprite;

//Helper Methods
- (void) setState: (int) stt;
- (void) manageSprites;
- (void) renderSprites;
- (int) getRndBetween: (int) bottom and: (int) top;
- (NSMutableDictionary*) getDictionary;
- (void) removeFromDictionary: (NSString*) name;

//OGL
- (void) setOGLProjection;
- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2;
- (void) drawOGLRect: (CGRect) rect;
- (void) drawOGLImg: (NSString*) picName at: (CGPoint) p;
- (void) drawOGLString: (NSString*) text at: (CGPoint) p;
- (CGSize) getOGLImgDimension: (NSString*) picName;
- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag;

@end
