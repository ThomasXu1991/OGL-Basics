
#import "Mine.h"
#import "GameManager.h"

@implementation Mine

- (void) additionalSetup {
    autoDestroy = true;
    sign = [[GameManager getInstance] getRndBetween: -1 and: 1]; 
    if (sign == 0) {
        sign = 1;
    }
}

- (void) renderSprite {
    angle+=(sign*5);
    [super renderSprite];
}

@end
