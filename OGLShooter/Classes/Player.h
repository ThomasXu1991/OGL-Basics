
#import "Sprite.h"

@interface Player : Sprite {    
    CGPoint touchPoint;
    bool touchAction;
    bool moveLeft;
    int speedScalar;  
    bool dead;
}

- (void) setTouch: (CGPoint) touchPoint;
- (void) touchEnded;
- (void) fire;

@end
