
#import "Tex.h"

@interface ParallaxLayer : NSObject {    
    Tex *tex;
    
    int layerW;
    int layerH;
    
    float refX;
    float refY; 

    float oldPx;
    float oldPy;
}

- (id) initWithPic: (NSString *) picName;
- (void) drawWithFactor: (float) factor 
             relativeTo: (CGPoint) pos
               atOrigin: (CGPoint) o;

@end