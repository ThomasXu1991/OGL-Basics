
#import "Bullet.h"
#import "GameManager.h"

@implementation Bullet

- (void) setAngle: (int) degree {
    angle = degree;
    autoDestroy = true;
}

- (void) hit {
    active = false;
}

@end