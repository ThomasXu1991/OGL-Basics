
#import <UIKit/UIKit.h>
#import "MainView.h"

@interface MyGameAppDelegate : NSObject <UIApplicationDelegate> {
    MainView *mainView;
}

@property (strong, nonatomic) UIWindow *window;

@end

