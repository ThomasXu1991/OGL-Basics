
#import <UIKit/UIKit.h>

extern int W; 
extern int H;

@interface MainView : UIView {
}

- (void) drawCircle: (int) x 
                  y: (int) y 
             radius: (int) radius 
                 gc: (CGContextRef) gc;
- (int) getRndBetween: (int) bottom and: (int) top;

@end