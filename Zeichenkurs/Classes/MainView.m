
#import "MainView.h"

int W=320;
int H=480;

@implementation MainView

- (void) drawRect: (CGRect) rect {    
    W = rect.size.width; 
    H = rect.size.height;
    
    CGContextRef gc = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(gc, 1, 1, 1, 1);
    CGContextSetRGBFillColor(gc, 1, 1, 1, 1);
	CGContextSetLineWidth(gc, 1.0);       

	CGContextMoveToPoint(gc, W/2, 0); 
	CGContextAddLineToPoint(gc, 0, H/2);
    CGContextAddLineToPoint(gc, W, H/2);
    CGContextAddLineToPoint(gc, W/2, 0);
	CGContextStrokePath(gc);
          
    CGContextMoveToPoint(gc, W/2, H/2);
    for (int i=0; i<10; i++) {
        int x = [self getRndBetween: 0 and: W];
        int y = [self getRndBetween: 0 and: H];
        CGContextAddLineToPoint(gc, x, y);        
    } 
 	CGContextDrawPath(gc, kCGPathStroke);
    
    /*
    CGRect rectangle = CGRectMake (100,100,50,50); 	
	CGContextAddRect(gc, rectangle);   
    CGContextDrawPath(gc, kCGPathFillStroke);
    
    [self drawCircle: 100 y:200 radius: 50 gc: gc];
    */  
    
    CGContextDrawPath(gc, kCGPathFillStroke);
         
    CGContextSetRGBFillColor(gc, 1, 1, 1, 1);
    NSString *str = @"Drawing Class";
    UIFont *uif = [UIFont fontWithName:@"Verdana-Italic" size: 40];
	[str drawAtPoint:CGPointMake(10, 10) withFont: uif]; 
}

- (void) drawCircle: (int) x 
                  y: (int) y 
             radius: (int) radius 
                 gc: (CGContextRef) gc {
	CGRect rect = CGRectMake (x-radius, y-radius, radius*2, radius*2); 	
	CGContextSetRGBFillColor(gc, 0,0,0,0);	
	CGContextAddEllipseInRect(gc, rect);
}

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
}  

@end
