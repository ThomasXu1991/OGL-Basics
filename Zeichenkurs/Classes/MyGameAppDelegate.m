
#import "MyGameAppDelegate.h"

@implementation MyGameAppDelegate

@synthesize window = _window;

// Note: You can ignore the Xcode console message "Application windows are expected to have a root view controller
// at the end of application launch". For simplicity most game projects in this book consist only of a single view.
// However, you can easily add a ViewController if you need it. See e.g. the GameKit or the GLKitBasics examples.

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    UIViewController *RootVC = [[UIViewController alloc] init];
    self.window.rootViewController = RootVC;
	mainView = [[MainView alloc] initWithFrame: [UIScreen mainScreen].applicationFrame]; 
	[RootVC.view addSubview: mainView];
    [self.window makeKeyAndVisible];      
    return YES;
}

- (void) applicationDidBecomeActive: (UIApplication *) application {}
- (void) applicationWillResignActive: (UIApplication *) application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {}

- (void)dealloc {
    [_window release];
    [mainView release];
    [super dealloc];
}

@end
