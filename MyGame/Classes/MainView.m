
#import "MainView.h"

int W=320;
int H=480;

@implementation MainView

- (void) drawRect: (CGRect) rect {  
    W = rect.size.width; 
    H = rect.size.height;
    NSLog(@"W: %i H: %i", W, H);
    
    CGContextRef gc = UIGraphicsGetCurrentContext();
	CGContextSetRGBFillColor(gc, 1, 1, 1, 1); 	    
    NSString *str = @"My Game";
    UIFont *uif = [UIFont systemFontOfSize:40];
	[str drawAtPoint:CGPointMake(10, 10) withFont: uif];    
}

@end
