
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MainView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    MainView *mainView;    
    id timer; //OS < 3.1 instanceOf NSTimer else instanceOf CADisplayLink   
}

@property (strong, nonatomic) UIWindow *window;

@end
