
#import <Foundation/Foundation.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SoundOAL : NSObject {	
	ALCcontext *context;
	ALCdevice *device;
	NSMutableArray *soundIDs;
    NSMutableDictionary *soundIDDictionary;
	NSMutableDictionary *soundBufferIDDictionary;   
}

+ (SoundOAL *) getInstance;
- (void) setupOpenAL;
- (void) logErrors: (OSStatus) status;
- (NSUInteger) findFreeSoundID;


- (void) loadSound: (NSString*) soundName 
                Hz: (NSUInteger) sampleRate;
- (void) playSound: (NSString*) soundName;
- (void) loopSound: (NSString*) soundName;
- (void) stopSound: (NSString*) soundName;

@end
