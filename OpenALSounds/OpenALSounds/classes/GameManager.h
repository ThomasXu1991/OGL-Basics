
#import <UIKit/UIKit.h>
#import "Sprite.h"
#import "Circle.h"
#import "SoundEvent.h"
#import "SoundOAL.h"

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    START_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	    
    UIImage *background;
    
    NSMutableArray *sprites;
    NSMutableArray *newSprites; 
    NSMutableArray *destroyableSprites;    
    NSMutableDictionary* dictionary; 
    
    int time; 
    NSMutableArray *sequencer; 
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;
- (void) createSprite: (int) type 
                speed: (CGPoint) sxy 
                  pos: (CGPoint) pxy;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) handleStates;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 
- (void) checkSprite: (Sprite *) sprite;

//Helper Methods
- (void) setState: (int) stt;
- (void) manageSprites;
- (void) renderSprites;
- (int) getRndBetween: (int) bottom and: (int) top;
- (void) drawString: (NSString *) str at: (CGPoint) p;
- (NSMutableDictionary*) getDictionary;
- (UIImage *) getPic: (NSString*) picName;

@end
