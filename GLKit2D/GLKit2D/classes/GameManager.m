
#import "GameManager.h"

int W=320;
int H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {        
    [self setOGLProjection];    
    state = LOAD_GAME;
}

- (void) loadGame { 
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            break;           
        default: NSLog(@"ERROR: Unknown state: %i", state);
            break;
    }    
}	 

- (void) playGame {    
    timer++;                 

    for (int i = 0; i < 100; i++) {
        int x = [self getRndBetween: 0 and: W];
        int y = [self getRndBetween: 0 and: H];
        [self drawGLKitImage: @"blueBird" at: CGPointMake(x, y)];
    }    
    
    /*
    for (int i = 0; i < 100; i++) {
        int x = [self getRndBetween: 0 and: W];
        int y = [self getRndBetween: 0 and: H];
        [self drawOGLRect: CGRectMake(x, y, 30, 30)];
    } 
    */
} 

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {
    //Set View
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();    
    glOrthof(0, W, H, 0, 0, 1); 
    
    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY); 
    glDisable(GL_DEPTH_TEST); //2D only
} 

- (void) drawOGLTriangle {    
    GLshort vertices[ ] = {
        0,   250, 
        250, 250, 
        0,   0,   
    };
    
    glColor4f(0, 0, 1, 1);
    glVertexPointer(2, GL_SHORT, 0, vertices);                         
    glDrawArrays(GL_TRIANGLES, 0, 3);   
}

- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2 {
    GLshort vertices[ ] = { 
        p1.x, p1.y,
        p2.x, p2.y        
    };      
    
    glColor4f(0, 0, 1, 1);
    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glDrawArrays(GL_LINES, 0, 2);       
}

- (void) drawOGLRect: (CGRect) rect {    
    GLshort vertices[ ] = {
        0,                  rect.size.height, 
        rect.size.width,    rect.size.height, 
        0,                  0, 
        rect.size.width,    0  
    };
        
    glColor4f(0, 0, 1, 1); 
    glVertexPointer(2, GL_SHORT, 0, vertices);              
    glPushMatrix();      
    glTranslatef(rect.origin.x, rect.origin.y, 0);          
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); 
    glPopMatrix();      
}

- (void) drawGLKitImage: (NSString*) name at: (CGPoint) p { 
	Tex *tex = [[self getDictionary] objectForKey: name];
	if (!tex) {
		tex = [[Tex alloc] init];         
        [tex createTexFromImage: name];              
        [[self getDictionary] setObject: tex forKey: name];
        [tex release];         
	}  
    [tex drawAt: p];
}

#pragma mark ============================= Helper Methods ===============================

- (void) setState: (int) stt {
    state = stt;
}

- (NSMutableDictionary *) getDictionary {
	if (!dictionary) { //Hashtable
		dictionary = [[NSMutableDictionary alloc] init]; 
	}
	return dictionary;
}

- (void) removeFromDictionary: (NSString*) name {
    [[self getDictionary] removeObjectForKey: name];
} 

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {  
    [dictionary release]; 
    [super dealloc];
}

@end
