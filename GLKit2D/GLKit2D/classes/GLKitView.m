
#import "GLKitView.h"

@implementation GLKitView

#pragma mark ============================= Init Methods ===============================

- (void) setupOGL {      
    eaglContext = [[EAGLContext alloc] initWithAPI: kEAGLRenderingAPIOpenGLES1];    
    if (!eaglContext || ![EAGLContext setCurrentContext: eaglContext]) {
        [self release];
    } else {            
        
        self.context = eaglContext;
        
        if (!gameManager) {
            gameManager = [GameManager getInstance];
        }          
    }               
}

#pragma mark ============================= Draw Method ===============================

- (void) drawRect: (CGRect) rect {     
     glClearColor(1, 1, 0, 1.0); 
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);       
     [gameManager drawStatesWithFrame: rect];     
}

#pragma mark ============================= Input Handling ===============================

- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event {	
    CGPoint p = [[touches anyObject] locationInView: self];     
    [gameManager touchBegan: p];  
}

- (void) touchesMoved: (NSSet *) touches withEvent: (UIEvent *) event {   
    CGPoint p = [[touches anyObject] locationInView: self];        
    [gameManager touchMoved: p];
}

#pragma mark ============================= Deallocation ===============================

-(void) dealloc {
    [gameManager release];        
    if (eaglContext) {
        [eaglContext release];
    }    
    [super dealloc];
}

@end
