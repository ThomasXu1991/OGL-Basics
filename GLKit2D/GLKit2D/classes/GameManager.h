
#import <UIKit/UIKit.h>
#import <OpenGLES/ES1/gl.h>
#import "Tex.h"

extern int W; 
extern int H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 
    int timer; 
    NSMutableDictionary* dictionary;    
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 

//Helper Methods
- (void) setState: (int) stt;
- (int) getRndBetween: (int) bottom and: (int) top;
- (NSMutableDictionary *) getDictionary;
- (void) removeFromDictionary: (NSString*) name;

//OGL
- (void) setOGLProjection;
- (void) drawOGLTriangle;
- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2;
- (void) drawOGLRect: (CGRect) rect;
- (void) drawGLKitImage: (NSString*) name at: (CGPoint) p;

@end
