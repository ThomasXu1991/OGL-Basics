
#import <GLKit/GLKit.h>
#import <OpenGLES/ES1/gl.h>

@interface Tex : NSObject {
    GLKTextureInfo *textureInfo;
    GLKEffectPropertyTexture *textureProperty;
    GLuint textureID;
    int width;
    int height;
}

- (void) createTexFromImage: (NSString *) picName;

- (void) drawAt: (CGPoint) p;

- (GLuint) getTextureID;
- (int) getWidth;
- (int) getHeight;

@end
