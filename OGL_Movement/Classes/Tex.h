
#import <OpenGLES/ES1/gl.h>

@interface Tex : NSObject {
    GLuint textureID;
    int width;
    int height;
}

- (void) createTexFromImage: (NSString *) picName;
- (void) createTexFromString: (NSString *) text;

- (GLubyte *) generatePixelDataFromImage: (UIImage *) pic;
- (GLubyte *) generatePixelDataFromString: (NSString *) text;
- (void) generateTexture: (GLubyte *) pixelData;

- (void) drawAt: (CGPoint) p;

- (GLuint) getTextureID;
- (int) getWidth;
- (int) getHeight;

@end
