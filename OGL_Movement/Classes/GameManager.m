
#import "GameManager.h"

int W=320;
int H=480;

@implementation GameManager

#pragma mark ============================= Init Methods ================================

+ (GameManager*) getInstance  {
    
    static GameManager* gameManager;
		
	if (!gameManager) {
        gameManager = [[GameManager alloc] init];
		[gameManager preloader];            
	}
	    
    return gameManager;
}

- (void) preloader {        
    [self setOGLProjection];    
    state = LOAD_GAME;
}

- (void) loadGame { 
}

#pragma mark ============================= Game Handler ===============================

- (void) touchBegan: (CGPoint) p {      
    NSLog(@"Touch: %f %f", p.x, p.y);
}

- (void) touchMoved: (CGPoint) p {
    [self touchBegan: p];      
}

- (void) touchEnded {
}

- (void) drawStatesWithFrame: (CGRect) frame { 
    W = frame.size.width;
    H = frame.size.height; 
    switch (state) {
        case LOAD_GAME: 
            [self loadGame];
            state = PLAY_GAME;
            break;  
        case PLAY_GAME:
            [self playGame];
            break;           
        default: NSLog(@"ERROR: Unknown state: %i", state);
            break;
    }    
}	 

- (void) playGame {
    timer++;          
    
    [self translate];
    [self rotate];
    [self scale];
    [self allTogether];   
} 

- (void) translate {
    static int x = 160 - 32;
    static int y = 480 - 32;
    static int speend = 10;
    y -= speend;
    if (y < 0) {
        speend = -speend;
    }else if(y > H){
        speend = -speend;
    }
    glPushMatrix();    
    glTranslatef(x, y, 0);
    //旋转－－start
    if (y > 0 && (speend > 0)) {
        glRotatef(0, 0, 0, 1);
    }else if(y < H && (speend < 0)){
        glTranslatef(x, y, 0);
        glRotatef(180, 0, 0, 1);
        glTranslatef(x - 64, y - 64, 0);
    }
    //旋转－－end
    [self drawOGLImg: @"player.png" at: CGPointMake(0, 0)];
    glPopMatrix(); 
}

- (void) rotate {
    static int a = 0;
    //角度
    a -= 10;
    
    glPushMatrix();
    glTranslatef(160, 240, 0); 
    glRotatef(a, 0, 0, 1);
    [self drawOGLImg: @"player.png" at: CGPointMake(0, 0)];  
    glPopMatrix(); 
}

- (void) scale {
    static float s = 1;
    static int sf = 1;
    
    s -= 0.01 * sf;    
    if (s < 0 || s > 1) {
        sf *= -1;
    }
    
    glPushMatrix();
    glTranslatef(160, 240, 0);
    glScalef(s, s, 1);    
    [self drawOGLImg: @"player.png" at: CGPointMake(0, 0)]; 
    glPopMatrix(); 
}

- (void) allTogether {
    static int x = 160;
    static int y = 480;
    static int a = 0;
    static float s = 1;
    static int sf = 1; 
    
    y -= 1; 
    a -= 10;
    
    s -= 0.01 * sf;    
    if (s < 0 || s > 1) {
        sf *= -1;
    }
    
    glPushMatrix();     
    glTranslatef(x, y, 0);    
    glRotatef(a, 0, 0, 1);
    glScalef(s, s, 1); 
    [self drawOGLImg: @"player.png" at: CGPointMake(0, 0)];         
    glPopMatrix(); 
}

#pragma mark ============================= OGL Methods ===============================

- (void) setOGLProjection {
    //Set View
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();    
    glOrthof(0, W, H, 0, 0, 1);
    
    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY); 
    glDisable(GL_DEPTH_TEST); //2D only
} 

- (void) drawOGLLineFrom: (CGPoint) p1 to: (CGPoint) p2 {
    GLshort vertices[ ] = { 
        p1.x, p1.y,
        p2.x, p2.y        
    };      

    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    glDrawArrays(GL_LINES, 0, 2);       
}

- (void) drawOGLRect: (CGRect) rect {    
    GLshort vertices[ ] = {
        0,                  rect.size.height, 
        rect.size.width,    rect.size.height, 
        0,                  0,
        rect.size.width,    0  
    };
        
    glVertexPointer(2, GL_SHORT, 0, vertices);           
    glColor4f(1, 1, 0, 1);
    
    glPushMatrix();      
    glTranslatef(rect.origin.x, rect.origin.y, 0);          
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); 
    glPopMatrix();      
}

- (void) drawOGLImg: (NSString*) picName at: (CGPoint) p {    
    Tex *tex = [self getTex: picName isImage: YES]; 
    if (tex) {
        [tex drawAt: p];
    }
}

- (CGSize) getOGLImgDimension: (NSString*) picName {    
    Tex *tex = [self getTex: picName isImage: YES]; 
    if (tex) {
        return CGSizeMake([tex getWidth], [tex getHeight]);
    }
    return CGSizeMake(0, 0); 
}

- (void) drawOGLString: (NSString*) text at: (CGPoint) p {    
    Tex *tex = [self getTex: text isImage: NO]; 
    if (tex) {
        [tex drawAt: p];
    }
}

- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag { 
	Tex *tex = [[self getDictionary] objectForKey: name];
	if (!tex) {
		tex = [[Tex alloc] init];
        if (imgFlag) {            
            [tex createTexFromImage: name];             
        } else {
            [tex createTexFromString: name];
        }    
        [[self getDictionary] setObject: tex forKey: name];
        [tex release];         
	}          
	return tex;  
}

#pragma mark ============================= Helper Methods ===============================

- (void) setState: (int) stt {
    state = stt;
}

- (NSMutableDictionary *) getDictionary {
	if (!dictionary) { //Hashtable
		dictionary = [[NSMutableDictionary alloc] init]; 
	}
	return dictionary;
}

- (void) removeFromDictionary: (NSString*) name {
    [[self getDictionary] removeObjectForKey: name];
} 

- (int) getRndBetween: (int) bottom and: (int) top {		
	int rnd = bottom + (arc4random() % (top+1-bottom)); 
	return rnd;
} 

- (void) dealloc {        
    [dictionary release];
    [super dealloc];
}

@end
