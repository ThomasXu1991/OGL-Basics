
#import "Zombie.h"

@interface Player : Zombie {
    CGPoint touchPoint;
}

- (void) setTouch: (CGPoint) touchPoint;

@end
