
#import "Player.h"

@implementation Player

- (void) draw {        
    [super draw];      
        
    //Stop movement
    if (speed.x > 0 && touchPoint.x < pos.x) speed.x = 0;
    if (speed.x < 0 && touchPoint.x > pos.x) speed.x = 0;
    if (speed.y > 0 && touchPoint.y < pos.y) speed.y = 0;
    if (speed.y < 0 && touchPoint.y > pos.y) speed.y = 0;    
}  

- (void) setTouch: (CGPoint) point {
    touchPoint = point;
    speed.x = (touchPoint.x - pos.x)/20; //deltaX
    speed.y = (touchPoint.y - pos.y)/20; //deltaY   
}

@end