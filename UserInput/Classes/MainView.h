
#import <UIKit/UIKit.h>
#import "Sprite.h"
#import "Zombie.h"
#import "Player.h"

extern int W; 
extern int H;

@interface MainView : UIView <UIAccelerometerDelegate> {   
    //NSMutableArray *sprites;
    UIAccelerometer *accelerometer;
    UIImage *background;
    Player *player;
    bool useSensor;
}

- (int) getRndBetween: (int) bottom and: (int) top;
- (void) accelerometer: (UIAccelerometer *) accelerometer 
         didAccelerate: (UIAcceleration *) acceleration;

@end