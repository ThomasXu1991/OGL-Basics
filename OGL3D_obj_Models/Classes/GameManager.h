
#import <UIKit/UIKit.h>
#import "Tex.h"

extern float W; 
extern float H;

enum states {
    LOAD_GAME,
    PLAY_GAME
}; 

@interface GameManager : NSObject {
    int state; 	
    
    NSMutableDictionary* dictionary; 
    
    float zNear; 
    float zFar; 
    float fieldOfViewAngle;
    
    int timer;
}

//Init Methods
+ (GameManager *) getInstance;
- (void) preloader;
- (void) loadGame;

//Game Handler
- (void) touchBegan: (CGPoint) p;
- (void) touchMoved: (CGPoint) p;
- (void) touchEnded;
- (void) drawStatesWithFrame: (CGRect) frame; 
- (void) playGame; 

//Helper Methods
- (int) getRndBetween: (int) bottom and: (int) top;
- (NSMutableDictionary*) getDictionary;
- (void) removeFromDictionary: (NSString*) name;

//OGL
- (void) setOGLProjection;
- (void) setLight;
- (void) drawCube;
- (void) drawMyYuan;
- (void) drawTorusKnot;
- (Tex *) getTex: (NSString*) name isImage: (bool) imgFlag;

@end
