
#import "MainView.h"

@implementation MainView

- (void) drawRect: (CGRect) rect {    
    CGContextRef gc = UIGraphicsGetCurrentContext();
	CGContextSetRGBFillColor(gc, 1, 1, 1, 1); 	
		
    NSString *str = @"Hello World";
    UIFont *uif = [UIFont systemFontOfSize:40];
	[str drawAtPoint:CGPointMake(50, 200) withFont: uif];    
}

@end
