
#import "HelloWorldAppDelegate.h"

@implementation HelloWorldAppDelegate

@synthesize window = _window;

// Note: You can ignore the Xcode console message "Application windows are expected to have a root view controller
// at the end of application launch". For simplicity most game projects in this book consist only of a single view.
// However, you can easily add a ViewController if you need it. See e.g. the GameKit or the GLKitBasics examples.

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor whiteColor];    
    mainView = [[MainView alloc] initWithFrame: [UIScreen mainScreen].applicationFrame];   
    [self.window addSubview: mainView];    
    [self.window makeKeyAndVisible];
            
    NSLog(@"Hello World!"); 
    
    int value = 22;
    NSLog(@"Value: %i", value); 
    
    NSString *myString = @"a String";
    float fvalue = 22.7;
    NSLog(@"Output: %f, String: %@, Window: %@", fvalue, myString, self.window); 
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {}
- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {}
- (void)applicationDidBecomeActive:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {}

- (void)dealloc {
    [_window release];
    [mainView release];
    [super dealloc];
}

@end
