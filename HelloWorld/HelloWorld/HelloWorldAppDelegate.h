
#import <UIKit/UIKit.h>
#import "MainView.h"

@interface HelloWorldAppDelegate : NSObject <UIApplicationDelegate> {
    MainView *mainView;    
}

@property (strong, nonatomic) UIWindow *window;

@end
