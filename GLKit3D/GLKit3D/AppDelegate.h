
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GLKitView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UIWindow *window; 
    GLKitView *glKitView;     
    id timer;  
}

- (void) startGameLoop;
- (void) stopGameLoop;
- (void) loop;

@property (strong, nonatomic) UIWindow *window;

@end
