
#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "GameManager.h"

@interface GLKitView : GLKView {   
    GameManager *gameManager;     
    EAGLContext *eaglContext; 
}

- (void) setupOGL;

@end
