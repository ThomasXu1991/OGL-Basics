
#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.window.backgroundColor = [UIColor whiteColor];
    UIViewController *RootVC = [[UIViewController alloc] init];
    self.window.rootViewController = RootVC;
    
    glKitView = [[GLKitView alloc] initWithFrame: [UIScreen mainScreen].applicationFrame]; 
    glKitView.backgroundColor = [UIColor grayColor];      
    [glKitView setupOGL];
    [RootVC.view addSubview: glKitView];
    [self.window makeKeyAndVisible];    
    return YES;
}

- (void) startGameLoop {    
    NSString *deviceOS = [[UIDevice currentDevice] systemVersion];    
    bool forceTimerVariant = TRUE;
    
    if (forceTimerVariant || [deviceOS compare: @"3.1" options: NSNumericSearch] == NSOrderedAscending) {
        //33 frames per second -> timestep between the frames = 1/33    
        NSTimeInterval fpsDelta = 0.0303;
        timer = [NSTimer scheduledTimerWithTimeInterval: fpsDelta
                                                 target: self
                                               selector: @selector( loop )
                                               userInfo: nil
                                                repeats: YES]; 
        
    } else {
        int frameLink = 2;
        timer = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget: self selector: @selector( loop )];
        [timer setFrameInterval: frameLink];
        [timer addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
    }
    
    NSLog(@"Game Loop timer instance: %@", timer);   
}

- (void) stopGameLoop {
    [timer invalidate];
    timer = nil;
}

- (void) loop {
    [glKitView setNeedsDisplay];
}

- (void) applicationDidBecomeActive: (UIApplication *) application {
    [self startGameLoop];
}

- (void) applicationWillResignActive: (UIApplication *) application {
    [self stopGameLoop];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {}
- (void)applicationWillEnterForeground:(UIApplication *)application {}
- (void)applicationWillTerminate:(UIApplication *)application {}

- (void)dealloc
{
    [self stopGameLoop];
    [timer release];
    [_window release];
    [super dealloc];
}

@end
